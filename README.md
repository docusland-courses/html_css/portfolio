---
title: Projet Profolio
metadata:
  level : first_year
  language: html, css, js, php

---

## Présentation du projet 


### Objet du document

Ce document s'adresse à une équipe de développeurs d'applications web en première année de formation


### Descriptions des parties prenantes

- Client / Chef de projet : Service pédagogique
- Développeur : Etudiant en 1e année

### Objectifs

L'équipe doit créer un site web mettant en avant un portfolio, celui-ci doit comporter un formulaire de contacts. 

### Description

Votre portfolio devra être réalisé sous forme de site web. Le site devra présenter l'ensemble des réalisations que vous avez à votre actif. Que ce soit des réalisations conçues au sein des cours, des liens vers vos dépôts git ou encore des réalisations réalisées par le passé à titre personnel. L'objectif ici de ce site est de vous représenter auprès des recruteurs. 

Il ne s'agit pas d'un CV en ligne, mais plutôt d'une démonstration de vos réalisations passées.

Pour chaque réalisation, tâchez d'y associer une description, permettant de savoir pourquoi, quand et comment vous avez réalisé ce projet.

Un portfolio a essentiellement deux types de cibles :

 - Le **recruteur RH**, qui sera plus attiré par l'esthétique du site et des mot clés technique que vous pourriez mentionner. 
 - Le **recruteur technique**, qui sera plus critique envers la propreté du code internet ainsi qu'aux références techniques que vous pourriez mentionner. 

En première année, vous n'avez pas forcément de nombreuses réalisations abouties à y représenter, mais vous aurez des références à y intégrer ultérieurement. Ainsi, s'il vous manque des informations, prenez une image fictive ainsi qu'un descriptif temporaire mentionnant le fait que le site est en cours ou que des réalisations sont à venir.

Afin de vous permettre d'y intégrer des nouveaux projets facilement ultérieurement, un stockage de vos projets au sein d'une base de données serait judicieux mais vous n'avez peut-être pas encore l'ensemble des compétences pour le réaliser ?

Si vous ne vous sentez pas prêts à côtoyer PDO, rédigez les projets en dur au sein d'un tableau dans le PHP.  Adaptez le site pour que les blocs se répètent. 

Il sera alors plus aisé, par la suite, de faire évoluer le tout avec une BDD et remplacer votre tableau.


### Description de l'existant

Il n'y a pas d'existant. Vous êtes libres de vous inspirer de projets existants.  Vous pouvez réaliser de la veille sur [dribble](https://dribbble.com/search/portfolio) ou encore [webflow](https://webflow.com/blog/design-portfolio-examples)

###  Critères d'acceptabilité du produit
 
 * Le(s) document(s) livrés doivent être respectueux de la RGPD.
 * Le(s) document(s) livrés doivent être responsive

## Expression de besoin

### Analyse fonctionnelle

Il est demandé de concevoir les supports suivants : 

  - Celui-ci doit faire la promotion de votre travail
  - Mettre en avant vos travaux personnels, en entreprise et lors de vos sessions de formation
  - Vous devez pouvoir intégrer différents contenus (texte, image, vidéos, ...) 
  - Il doit être possible de vous contacter par le biais d'un formulaire sur votre site internet.  L'envoi de l'email réel étant optionnel. Si vous ne savez comment procéder, vous pouvez intégrer un google form, wufoo ou encore typeform.

### Analyse non fonctionnelle
 
 - Votre application doit être accessible en français au minimum
 - L'application doit s'adapter aux différents supports informatiques (responsive)

### Les livrables du projet

L'équipe devra fournir :

 - Le zoning de l'application
 - Un wireframe ou encore une maquettage de l'application
 - Un prototype HTML/CSS (optionnel Javascript)
 - Ainsi qu'un document de gestion de projet incluant les reporting d'activités. Celà peut être à minima un RAF au format excel listant les tâches, et la répartition fait / à faire / prévisionnel. Ou directement un accès à git où à l'outil de gestion de projet utilisé. 
- un projet complet et fonctionnel
- il est possible de nous communiquer une URL si vous avez déployé le site sur un support de votre choix.

### Coûts

Il n'y a pas d'enveloppe budgetaire compte tenu de la teneur pédagogique du projet.

### Conseils

 - Vous pouvez utiliser des frameworks. 
 - Le contenu que vous intégrerez devra être travaillé, soigné et professionnel. Ne partez pas sur la représentation de 10 projets dès le début. Faites en un de manière abouttie. Puis, une fois totalement satisfait listez d'autres projets. 
 - On ne se lance jamais sur un projet tête baissée, refléchissez : que pouvez-vous intégrer dans ce site ? Ou que pourrez-vous y intégrer ? Est-ce que celà sera "rangé" dans des sections. Ou encore, autre que la technique pure, avez-vous d'autres réalisations à mettre en avant ? Avez-vous des liens vers d'autres plateformes ou d'autres documents à ajouter ? etc...

Que la force soit avec vous !
